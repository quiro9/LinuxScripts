LinuxScripts by quiro9
============================

###  My scripts for Distributions GNU/Linux , It's writing in lenguage bash. All is under GNU/GPL 3. Enjoy!


Installation
--------------------
* Dowload the "github.zip"
* Copy script in /usr/bin.
* Set to as preferred ( following the instructions given below )
* Most scripts were written for X11 and gnome, they are not guaranteed to work under wayland


Descriptions
--------------------

__________________
ForArch/pacman-orphan: a simple command for clear orphan packages in ArchLinux
ForArch/python: Script to exec corresponding python versión:
		> move /usr/bin/python and /bin/python were python.old
		> copy this script in /usr/bin/python and /bin/python

__________________
wine/fbx-conv (old):
* This is a transitional script (use wine) for fbx-conv.exe. If you using cocos2D-x in Linux you can need it script.
* You need wine (compatible whit fbx-conv.exe) and copy fbx-conv and fbx-conv.exe in /usr/bin/
* Please before using this script, you need configure wine for Windows 7, 8 or 10 apps

__________________
x11/display-force-on-x11:
* This Script Force to keep powered up the screen before the execution of a process or fullscreen ( eg flash -player )
* From process use you need greep, ps and xset.
* From fullscreen detection you need: greep, xdotool and xwininfo.
* The execution is automatic , it can be placed when the system starts

_________________
x11/segunda_pantalla_vnc-x11:
* This Script create a virtual create a virtual space desktop and share this via VNC.
* it have a many bugs but works under X11

__________________
angularNew.sh
* You can create an angular project for linux, it will create a script inside the folder, you can execute it by terminal (whether internal in a program or a favorite linux terminal), once executed it will remain in a loop, just press \<enter\> to recompile and run server or press \<c\> and \<enter\> to create a new component \<q\> and \<enter\> to exit.

__________________
mvcNew.sh
* You can create an MVC project for linux, it will create a script inside the folder, you can execute it by terminal (whether internal in a program or a favorite linux terminal), once executed it will remain in a loop, just press \<enter\> to recompile or press \<q\> and \<enter\> to exit.

__________________
x11/touchpad-disabledtyped-q9-x11.py
* This script uses python3
* You need to set the device names and time for it to work.
* When the configuration data is completed, you add it when you start the system, it will automatically capture the keys you press to activate or deactivate the touch panel and avoid accidental clicks and movements.

__________________
touchpad-state-q9
* this script enabled or disabled touchpad. When the flag is 1 power off the tocuhpad.
* You need ximput for no "gnome" desktops.
* The execution requires called ( Keyboard Cell example < Fn+Touchpad_key > )

touchpad-read-q9
* this script only read but not change and not notify the state of touchpad.
* this is for you start whit your system and it can remember the last touchpad state

__________________
x11/xrandread-q9-x11
* This script read the screen whit xrand and autoconfigure this output
* this is a modification of: https://github.com/Snifer/I3-WM/blob/master/autoxrandr

		

