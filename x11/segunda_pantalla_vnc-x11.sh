#!/bin/sh
## touchpad-read-q9 esta licenciado bajo GNU/GPL v3, realizado por quiro9.
## puede modificarlo libremente bajo los terminos de dicha licencia más información: http://www.gnu.org/licenses/gpl-3.0.html .

## esta testeado bajo X11 no funciona con Wayland correctamente se requiere otro servicio VNC y cambiar Xrandr

## la información obtenida segun la genración debe ser copiada y pegada para xrandr --newmode <info>
gtf 1366 1024 60
#gtf 1920 1080 60

## monitor virtual por xrandr (puede jugar con los parametros pero posiblemente se rompa)
xrandr --newmode "1368x1024_60.00"  116.52  1368 1456 1600 1832  1024 1025 1028 1060  -HSync +Vsync
#xrandr --newmode "1920x1080_60.00"  172.80  1920 2040 2248 2576  1080 1081 1084 1118  -HSync +Vsync

## info de lo generado
xrandr

## añadiendo pantalla virtual
xrandr --addmode HDMI-1 1366x1024_60.00
#xrandr --addmode HDMI-1 1920x1080_60.00

## posicionando pantalla a la derecha
xrandr --output HDMI-1 --mode 1366x01024_60.00 --right-of eDP-1
#xrandr --output HDMI-1 --mode 1920x1080_60.00 --right-of eDP-1

## iniciando servicio VNC con las proporciones y fuera de la resolución actual (el +1920 deberia ser cambiado por su resolucion de pantalla) 
x11vnc -clip 1366x1024+1920+0
#x11vnc -clip 1920x1080+1920+0
