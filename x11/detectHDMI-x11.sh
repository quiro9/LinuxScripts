#!/bin/bash

xpreviusState=1
xchangeState=0
while true; do
	hdmi_on=`xrandr | grep -c "HDMI-1 connected"`
	if [ $hdmi_on -eq 1 ]; then
		xchangeState=0
		if [ ${xpreviusState} == ${xchangeState} ]; then
	  		pacmd "set-card-profile 0 output:hdmi-stereo"
  		fi
	else
		xchangeState=1
		if [ ${xpreviusState} == ${xchangeState} ]; then
	 		pacmd "set-card-profile 0 output:analog-stereo+input:analog-stereo" && pulseaudio --kill && pulseaudio --start
		fi
	fi
	xpreviusState=${xchangeState}
	sleep 4
done
